import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Image, Text, View, TextInput, TouchableOpacity, KeyboardAvoidingView, Dimensions, Alert } from 'react-native';
import { MaterialIcons, Octicons } from  '@expo/vector-icons';
import { Card, ListItem, Button } from 'react-native-elements'

import api from '../services/api';

function SingUp({ navigation }) {

    return (
        <>
            <KeyboardAvoidingView style={styles.background}>
                <View style={styles.containerLogo}>
                    <Image source={require('/App/JWTproject/jwtProject/assets/LogoBranca.png')} style={{width: 250, height: 100, marginLeft: -8}}/>
                </View>
                
                <View style={styles.containerMain}>
                    <Card containerStyle={styles.cardMain}>
                        <Text style={styles.textEmailMain}>Login</Text>
                        <TextInput style={styles.inputEmailMain} autoCorrect={false} value={username} onChangeText={setUsername}>
                            
                        </TextInput>
                        <Text style={styles.textPasswordMain} >Senha</Text>
                        <TextInput style={styles.inputPasswordMain} autoCorrect={false} value={password} onChangeText={setPassword}></TextInput>
                        
                        <TouchableOpacity style={styles.buttonSignInMain} onPress={verifyUsers}>
                            <Text style={styles.textButtonSignInMain}>Acessar</Text>
                        </TouchableOpacity>
                        
                        <View style={styles.viewButtonsCreateMain}>
                            <Text style={styles.textCreateMainOne}>Não possui conta?</Text>
                            <TouchableOpacity style={styles.buttonCreateMain}>
                                <Text style={styles.textCreateMainTwo}> Crie uma</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>    
                </View>
            </KeyboardAvoidingView>
        </>
    );
}

const styles = StyleSheet.create({
    
})

export default Login;