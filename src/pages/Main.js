import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Text, TouchableOpacity, ImageBackground } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { Ionicons, MaterialCommunityIcons, AntDesign } from  '@expo/vector-icons';
import { Drawer, Header } from 'native-base';




const data = [
    { key: 'FABRICAÇÃO', id: 1 }, { key: 'LOGÍSTICA', id: 2 }, { key: 'MONTAGEM', id: 3 },
];

const formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);

    while(numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0){
        data.push({key: `blank-${numberOfElementsLastRow}`, empty: true});
        numberOfElementsLastRow = numberOfElementsLastRow + 1;
    }
    
    return data;
}

const numColumns = 3;



class SideBar extends Component {
    render() {
      return(
        <>
          <ImageBackground source={require('C:\\App\\JWTproject\\jwtProject\\assets\\backgorundSideMenu.png')} style={ styles.imageBackgroundSideMenu } >
            
          </ImageBackground>
        </>
      );
    }
  }


export default class Main extends Component {

    closeDrawer = () => {
        this.drawer._root.close()
      };
    openDrawer = () => {
    this.drawer._root.open()
    };

    renderItem = ({ item, index }) => {
        if(item.id == 1){
            return(
                <TouchableOpacity style={styles.item}>
                    <View style={styles.item}>
                        <Ionicons name="ios-construct" size={25} color={"#4d94ff"}/>
                        <Text style={styles.itemText}>
                            {item.key}
                        </Text>
                    </View>
                </TouchableOpacity>
            );
        } else if(item.id == 2) {
                return(
                    <TouchableOpacity style={styles.item}>
                        <View style={styles.item}>
                            <MaterialCommunityIcons name="truck-fast" size={25} color={"#4d94ff"}/>
                            <Text style={styles.itemText}>
                                {item.key}
                            </Text>
                        </View>
                    </TouchableOpacity>
                );
        } else if(item.id == 3) {
            return(
                <TouchableOpacity style={styles.item}>
                    <View style={styles.item}>
                        <AntDesign name="CodeSandbox" size={25} color={"#4d94ff"}/>
                        <Text style={styles.itemText}>
                            {item.key}
                        </Text>
                    </View>
                </TouchableOpacity>
            );
    }
        
            
    };

    render(){
        return (
            <>
                <Drawer
                ref={(ref) => { this.drawer = ref; }}
                content={<SideBar navigator={this.navigator} />}
                onClose={() => this.closeDrawer()}
                >
                    <ImageBackground source={require('C:\\App\\JWTproject\\jwtProject\\assets\\backgorundSideMenu.png')} style={ styles.imageBackgroundSideMenu } >

                            <View style={styles.sideBarView}>
                                <MaterialCommunityIcons style={styles.sideBarBoxHam} onPress={() => this.openDrawer()} name='menu' size={40} color="#4d94ff" />
                            </View>
                        <View style={styles.main}>
                            <View style={styles.header}>
                                
                            </View>
                            <Text style={styles.textTitle}>Planejamento e Controle</Text>
                            <FlatList 
                            data={formatData(data, numColumns)}
                            style={styles.container}
                            renderItem={(this.renderItem)}
                            numColumns={numColumns}
                            />
                        </View>
                    </ImageBackground> 
                </Drawer>
            </>
        );
    }
    
}

const styles = StyleSheet.create({
    header: {
        width: Dimensions.get('window').width,
        height: 70,
        marginTop: 30
    },
    sideBarBoxHam: {
        marginTop: 10,
        marginLeft: -180,

    },
    item: {
        borderRadius: 3,
        backgroundColor: '#e6e6e6',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 3,
        height: (Dimensions.get('window').width / numColumns) - 10,
    },
    itemText: {
        marginTop: 20,
    },
    main: {
        marginHorizontal: 10,
        flex: 1,
    },
    textTitle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 70,
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    imageBackgroundSideMenu: {
        width: '100%',
    height: '100%',
    position: 'absolute'
    },
})
