import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Image, Text, View, TextInput, TouchableOpacity, KeyboardAvoidingView, Dimensions, Alert } from 'react-native';
import { MaterialIcons, Octicons } from  '@expo/vector-icons';
import { Card, ListItem, Button } from 'react-native-elements'

import api from '../services/api';

function Login({ navigation }) {
    const [ users, setUsers ] = useState([]);
    const [ nome, setNome ] = useState('');
    const [ senha, setSenha ] = useState('');

    async function verifyUsers() {
        console.log(nome, senha);

        

        const response = await api.get('/autenticacao', {
            params: {
                nome,
                senha,
            }
        });

        if(nome == response.data.nome && senha == response.data.senha)
        {
            navigation.navigate('Main');
        } else if(username == "" || password == "") 
        {
            Alert.alert(
                'ERRO DE AUTENTICAÇÃO',
                'Preencha os dois campos para entrar',
                [
                    {
                      text: 'OK', onPress: () => console.log('OK Pressed')
                    },
                ],
                {
                    cancelable: false
                },
            );
        } 
        else
        {
            Alert.alert(
                'LOGIN OU SENHA INVÁLIDOS',
                'Tente novamente',
                [
                    {
                      text: 'OK', onPress: () => console.log('OK Pressed')
                    },
                ],
                {
                    cancelable: false
                },
            );
        }
        console.log(response.data.username);

    }

    return (
        <>
            <KeyboardAvoidingView style={styles.background}>
                <View style={styles.containerLogo}>
                    <Image source={require('/App/JWTproject/jwtProject/assets/LogoBranca.png')} style={{width: 250, height: 100, marginLeft: -8}}/>
                </View>
                
                <View style={styles.containerMain}>
                    <Card containerStyle={styles.cardMain}>
                        <Text style={styles.textEmailMain}>Login</Text>
                        <TextInput style={styles.inputEmailMain} autoCorrect={false} value={nome} onChangeText={senha}>
                            
                        </TextInput>
                        <Text style={styles.textPasswordMain} >Senha</Text>
                        <TextInput style={styles.inputPasswordMain} autoCorrect={false} value={senha} onChangeText={senha}></TextInput>
                        
                        <TouchableOpacity style={styles.buttonSignInMain} onPress={verifyUsers}>
                            <Text style={styles.textButtonSignInMain}>Acessar</Text>
                        </TouchableOpacity>
                        
                        <View style={styles.viewButtonsCreateMain}>
                            <Text style={styles.textCreateMainOne}>Não possui conta?</Text>
                            <TouchableOpacity style={styles.buttonCreateMain}>
                                <Text style={styles.textCreateMainTwo}> Crie uma</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>    
                </View>
            </KeyboardAvoidingView>
        </>
    );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },

    containerLogo: {
        height: 270,
        width: Dimensions.get('window').width,
        justifyContent: 'flex-end',
        paddingBottom: 100,
        alignItems: 'center',
        backgroundColor: '#4d94ff',
    },

    containerMain: {
        flex: 1,
        position:  'relative',
        bottom: 60,
        width: 350,
    },

    cardMain: {
        borderRadius: 7,
        height: 350,
        paddingVertical: 40
    },


    textEmailMain: {
        color: '#999999',
    },
    inputEmailMain: {
        borderBottomWidth: 1,
        //borderBottomColor: '#ff3333',
        borderBottomColor: '#8c8c8c',
        height: 40,
        marginBottom: 25
    },


    textPasswordMain: {
        color: '#999999',
    },
    inputPasswordMain: {
        borderBottomWidth: 1,
        //borderBottomColor: '#ff3333',
        borderBottomColor: '#8c8c8c',
        height: 40
    },


    buttonSignInMain: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#4d94ff',
        width: 200,
        height: 40,
        borderRadius: 25,
        left: 45,
        marginTop: 40
    },
    textButtonSignInMain: {
        color: 'white',
    },


    viewButtonsCreateMain: {
        flexDirection:'row', 
        flexWrap:'wrap', 
        alignItems: 'flex-start', 
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonCreateMain: {

    },
    textCreateMainOne: {
        color: '#999999',
    },
    textCreateMainTwo: {
        color: '#002966',
        fontWeight: 'bold'
    },
})

export default Login;