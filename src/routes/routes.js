import { createAppContainer } from  'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Login from '../pages/Login';
import Main from '../pages/Main';
import { NONE } from 'apisauce';

const Routes = createAppContainer(
    createStackNavigator({
        Login,
        Main,
    }, {
        defaultNavigationOptions: {
            headerShown: false,
        },
    })
);

export default Routes;