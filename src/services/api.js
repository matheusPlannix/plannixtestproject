import axios from 'axios';

const api = axios.create({
    baseURL:  'http://api-plannix.com.br/apigerencial/api'
});

export default api;