import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Routes from './src/routes/routes';

export default class App extends Component {
  render() {
    return (
      <>
          <Routes />
      </>
    );
  }
}
